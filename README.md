# DeepGlance Software Development Workflow

This workflow is a logical sequence of actions to be taken during the entire lifecycle of the software development process. GitLab will be used as platform that hosts the code.

The goal is to develop high quality software from the first stage of implementing to the last stage deploying to production. 

## Stages of Software Development
The software development process passes through the following steps:

- REQUIREMENT/FEATURE: The workflow starts from the needs to satisfy a requirement or to add a feature.

- ISSUE: The first step is to create an issue in GitLab. In this way the team can discuss about the best way to proceed.

- PLAN: Once the discussion comes to an agreement, the workflow is be prioritized and organized. For this, the *Gitlab Issue Board* tool is used.

- CODE: Once the workflow is organized a feature-branch is created and the code and the tests are written.

- COMMIT: Once the code is ready, it is committed into the *feature-branch*.

- TEST: CI automatically run the scripts to build and test the application.

- REVIEW: A merge request must be open as soon as the code is ready to be discussed. Once the build and the tests succeeds, the branch must be reviewed, approved and merged into the *master*.

- STAGING: The code is deployed to a staging environment to check if everything worked as we were expecting or if we still need adjustments.

- PRODUCTION: If the staging step succeed the code is deployed in the production environment.

- FEEDBACK: Check what stage of our work needs improvement.

# Issue tracking
The issue tracker allows the team to share and discuss ideas, before and while implementing them.

Issues are used for:
- Discussing ideas
- Submitting feature proposals
- Asking questions
- Reporting bugs and malfunction
- Obtaining support
- Elaborating new code implementations

It is important to make the relation between the code and the issue tracker transparent.
Any significant change to the code should start with an issue where the goal is described. Having a reason for every code change is important to inform everyone on the team and to help people keep the scope of a feature branch small. Each change to the codebase starts with an issue in the issue tracking system. If there is no issue yet it should be created first provided there is significant work involved. *Issue titles should describe the desired state of the system*.

When you are ready to code you start a branch for the issue from the *master* branch. The name of this branch should start with the issue number, for example `15-require-a-password-to-change-it`.

When you are done or want to discuss the code you open a *merge request*. This is an online place to discuss the change and review the code. Opening a merge request is a manual action since you do not always want to merge a new branch you push, it could be a long-running environment or release branch. If you open the merge request but do not assign it to anyone it is a 'Work In Progress' merge request. These are used to discuss the proposed implementation but are not ready for inclusion in the master branch yet. Start the title of the merge request with `[WIP]` or `WIP:` to prevent it from being merged before it's ready.

When the author thinks the code is ready the merge request is assigned to reviewer. The reviewer presses the merge button when they think the code is ready for inclusion in the master branch. In this case the code is merged and a merge commit is generated that makes this event easily visible later on. Merge requests always create a merge commit even when the commit could be added without one. This merge strategy is called *no fast-forward* in git. After the merge, the feature branch is deleted since it is no longer needed.

Suppose that a branch is merged but a problem occurs and the issue is reopened. In this case it is no problem to reuse the same branch name since it was deleted when the branch was merged. At any time there is at most one branch for every issue. It is possible that one feature branch solves more than one issue.

### Due dates
Every issue enables you to attribute a due date to it. It's important to have a way to set up a deadline for implementations and for solving problems. This can be facilitated by the due dates.

When you have due dates for multi-task projects—for example, a new release, product launch, or for tracking tasks by quarter—you can use milestones.

### Assignee
Whenever someone starts to work on an issue, it can be assigned to that person. You can change the assignee as much as you need. The idea is that the assignee is responsible for that issue until he/she reassigns it to someone else to take it from there.
It also helps with filtering issues per assignee.

### Labels
Labels are an important part of the flow. Are used to categorize the issues, to localize them in the workflow, and to organize them by priority with *Priority Labels*.
Labels facilitate the plan stage and organizing your workflow.

### Milestones
Milestones are the best tool to track the work of the team based on a common target, in a specific date.
The goal can be different for each situation, but the panorama is the same: you have a collection of issues and merge requests being worked on to achieve that particular objective.
This goal can be basically anything that groups the team work and effort to do something by a deadline. For example, publish a new release, launch a new product, get things done by that date, or assemble projects to get done by year quarters.

# Branch Flow
![Branch Flow](https://docs.gitlab.com/ee/university/training/gitlab_flow/feature_branches.png "The Branch Flow")

The GitHub flow will be used as base reference. This flow has only feature branches and a master branch. This is very simple and clean, many organizations have adopted it with great success. Atlassian recommends a similar strategy although they rebase feature branches. Merging everything into the master branch and deploying often means you minimize the amount of code in *inventory* which is in line with the lean and continuous delivery best practices.

# Merge/pull requests
Merge request are used to share intermediate results with the rest of the team. This can be done by creating a merge request without assigning it to anyone, instead you mention people in the description or a comment (`/cc @mark @susan`). This means it is not ready to be merged but feedback is welcome. Your team members can comment on the merge request in general or on specific lines with line comments. The merge requests serves as a code review tool and no separate tools such as Gerrit and reviewboard should be needed. If the review reveals shortcomings anyone can commit and push a fix. Commonly the person to do this is the creator of the merge/pull request. The diff in the merge/pull requests automatically updates when new commits are pushed on the branch.

When you feel comfortable with it to be merged you assign it to the person that knows most about the codebase you are changing and mention any other people you would like feedback from. There is room for more feedback and after the assigned person feels comfortable with the result the branch is merged. If the assigned person does not feel comfortable they can close the merge request without merging.

**The master branch is protected by commit** so you cannot commit directly, **you must always create a merge request**.

# WIP Merge Request
A `WIP MR`, which stands for *Work in Progress Merge Request*, is a technique we use to prevent that MR from getting merged before it's ready. Just add `WIP:` to the beginning of the title of an MR, and it will not be merged unless you remove it from there.
When your changes are ready to get merged, remove the `WIP:` pattern either by editing the issue and deleting manually, or use the shortcut available for you just below the MR description.

# Review
Once you've created a merge request, it's time to get feedback from your team or collaborators. Using the diffs available on the UI, you can add inline comments, reply to them and resolve them.

You can also grab the link for each line of code by clicking on the line number.
The commit history is available from the UI, from which you can track the changes between the different versions of that file. You can view them inline or side-by-side.

# Linking and closing issues from merge requests
The issues are closed once code is merged into the default branch.
If you only want to make the reference without closing the issue you can also just mention it.

# Pushing and removing branches
Push their feature branches frequently, even when they are not ready for review yet. By doing this you prevent team members from accidentally starting to work on the same issue. Of course this situation should already be prevented by assigning someone to the issue in the issue tracking software. However sometimes one of the two parties forgets to assign someone in the issue tracking software.

After a branch is merged it should be removed from the source control software. In GitLab and similar systems this is an option when merging. This ensures that the branch overview in the repository management software shows only work in progress. This also ensures that when someone reopens the issue a new branch with the same name can be used without problem. When you reopen an issue you need to create a new merge request.

# Committing often and with the right message
Commit early and often. Each time you have a functioning set of tests and code a commit can be made. The advantage is that when an extension or refactor goes wrong it is easy to revert to a working version. This is quite a change for programmers that used SVN before, they used to commit when their work was ready to share. The trick is to use the merge/pull request with multiple commits when your work is ready to share.

**The commit message must reflect your intention, not the contents of the commit**.

The contents of the commit can be easily seen anyway, the question is why you did it. An example of a good commit message is: *"Combine templates to dry up the user views."*. Some words that are bad commit messages because they don't contain much information are: *change, improve and refactor*. The word *fix or fixes* is also a red flag, unless it comes after the commit sentence and references an issue number. To see more information about the formatting of commit messages please see this great blog post by Tim Pope.

# Testing before merging
Developers had to ensure their code did not break the master branch. When using Developers create their branches from this master branch so it is essential it is green. Therefore each merge request must be tested before it is accepted. CI software shows the build results right in the merge request itself to make this easy.

 One drawback is that they are testing the feature branch itself and not the merged result. What one can do to improve this is to test the merged result itself. The problem is that the merge result changes every time something is merged into master. Retesting on every commit to master is computationally expensive and means you are more frequently waiting for test results. If there are no merge conflicts and the feature branches are short lived the risk is acceptable. If there are merge conflicts you merge the master branch into the feature branch and the CI server will rerun the tests. If you have long lived feature branches that last for more than a few days you should make your issues smaller.

# Acceptance criteria and coverage
At least the 80% of the source code should be covered by unit-testing, end every single test should run without failure as a general acceptance criteria. 

# Working with feature branches
When initiating a feature branch, always start with an up to date master to branch off from. If you know beforehand that your work absolutely depends on another branch you can also branch from there. If you need to merge in another branch after starting explain the reason in the merge commit.

If you have not pushed your commits to a shared location yet you can also rebase on master or another feature branch. Do not merge in upstream if your code will work and merge cleanly without doing so, Linus even says that you should never merge in upstream at random points, only at major releases. Merging only when needed prevents creating merge commits in your feature branch that later end up littering the master history.

